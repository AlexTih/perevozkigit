package ru.ice_aqua.perevozki.viewHolders;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.fragments.RedactirovanieFiltraFragment;

/**
 * Created by александр on 09.11.2017.
 */

public class ViewHoler  extends BaseViewHolder {

    TextView textView;

    public ViewHoler(ViewGroup parent) {
        super(R.layout.custom_card_view, parent);
        textView = (TextView) itemView.findViewById(R.id.tv_redactor);
    }
    public void bind(final FragmentTransaction fragmentTransaction){
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               fragmentTransaction.replace(R.id.frame, new RedactirovanieFiltraFragment());
                fragmentTransaction.commit();
            }
        });
    }

}