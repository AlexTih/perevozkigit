package ru.ice_aqua.perevozki.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by александр on 09.11.2017.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(int layoutResId, ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false));
    }

}