package ru.ice_aqua.perevozki.viewHolders;

import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import ru.ice_aqua.perevozki.R;

/**
 * Created by александр on 09.11.2017.
 */

public class ViewHolderPredlojenie  extends BaseViewHolder {

    ImageView imageView;

    public ViewHolderPredlojenie(ViewGroup itemView) {
        super(R.layout.custom_card_predlojenie, itemView);

        imageView = (ImageView) itemView.findViewById(R.id.iv_custom_card_predlojenie);
    }
}