package ru.ice_aqua.perevozki.viewHolders;

import android.view.ViewGroup;

import ru.ice_aqua.perevozki.R;

/**
 * Created by александр on 09.11.2017.
 */

public class ViewHolderVRabote extends BaseViewHolder {
    public ViewHolderVRabote(ViewGroup parent) {
        super(R.layout.custom_card_v_rabote, parent);
    }
}