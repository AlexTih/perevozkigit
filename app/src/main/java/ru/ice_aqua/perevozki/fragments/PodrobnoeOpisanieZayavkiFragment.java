package ru.ice_aqua.perevozki.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;


public class PodrobnoeOpisanieZayavkiFragment extends BaseFragment {


    public PodrobnoeOpisanieZayavkiFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initView("ПОДРОБНОЕ ОПИСАНИЕ ЗАЯВКИ");

        return inflater.inflate(R.layout.fragment_podrobnoe_opisanie_zayavki, container, false);
    }

}
