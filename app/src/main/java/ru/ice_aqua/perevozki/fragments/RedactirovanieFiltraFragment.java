package ru.ice_aqua.perevozki.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;


public class RedactirovanieFiltraFragment extends BaseFragment {


    View view;
    TextView textView2, textView3;
    ImageButton minus, plus;
    Button button;
    EditText editText, editText2, editText3;
    ImageView imageView;

    public RedactirovanieFiltraFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_redactirovanie_filtra, container, false);
        initView("РЕДАКТИРОВАНИЕ ФИЛЬТРА");
        init();

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double i = Double.parseDouble(textView2.getText().toString());
                String s = new DecimalFormat("#0.00").format(i + 0.10).replace(',', '.');
                textView2.setText(s);
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double i = Double.parseDouble(textView2.getText().toString());
                if(i>0) {
                    String s = new DecimalFormat("#0.00").format(i - 0.10).replace(',', '.');
                    textView2.setText(s);
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageView.getDrawable() == getActivity().getResources().getDrawable(R.drawable.checkbox_empty))
                    imageView.setImageResource(R.drawable.checkbox);
                else
                    imageView.setImageResource(R.drawable.checkbox_empty);
            }
        });

        return view;
    }

    public void init(){
        textView2 = (TextView) view.findViewById(R.id.redaktirovanie_current_price);
        textView3 = (TextView)  view.findViewById(R.id.redaktirovanie_current_price);
        minus = (ImageButton)  view.findViewById(R.id.redatirovanie_minus);
        plus = (ImageButton)  view.findViewById(R.id.redatirovanie_plus);

        button = (Button)  view.findViewById(R.id.redaktirovanie_create_filter_login);

        editText = (EditText)  view.findViewById(R.id.redaktirovanie_filter_from);
        editText2 = (EditText)  view.findViewById(R.id.redaktirovanie_filter_radius);
        editText3 = (EditText)  view.findViewById(R.id.redaktirovanie_filter_to);

        imageView = (ImageView) view.findViewById(R.id.redaktirovanie_iv_check_box);


        Spinner spinner = (Spinner)  view.findViewById(R.id.redaktirovanie_spinner);
        String[] years = {"1","2","3","4"};
        ArrayAdapter<CharSequence> langAdapter = new ArrayAdapter<CharSequence>(getContext(),R.layout.spinner_text, years );
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner.setAdapter(langAdapter);
    }

}
