package ru.ice_aqua.perevozki.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;

public class ChatFragment extends BaseFragment {


    public ChatFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initView("ПРЕДЛОЖЕНИЕ О РАБОТЕ");
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

}
