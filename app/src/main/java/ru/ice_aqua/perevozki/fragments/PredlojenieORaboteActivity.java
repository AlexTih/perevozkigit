package ru.ice_aqua.perevozki.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.ice_aqua.perevozki.BaseActivity;
import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.adapters.AdapterRvPredlojenie;


public class PredlojenieORaboteActivity extends BaseFragment {

    View view;
    RecyclerView mRecyclerView;
    AdapterRvPredlojenie adapter;
    TextView textView2, textView3, textView4;

    public PredlojenieORaboteActivity() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_predlojenie_orabote, container, false);
        initView("ПРЕДЛОЖЕНИЕ О РАБОТЕ");

//        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        actionBar.setTitle("ppp");
//        actionBar.set

        init();

        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, new PodrobnoeOpisanieZayavkiFragment());
                fragmentTransaction.commit();
            }
        });


        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, new PodpisZayavkiFragment());
                fragmentTransaction.commit();
            }
        });
        return view;
    }

    public void init(){

        textView2 = (TextView) view.findViewById(R.id.header_tv_predlojenie);
        textView2.setText("В работе 3 авто перейти в раздел мои заказы");

        textView3 = (TextView) view.findViewById(R.id.aaa);
        textView4 = (TextView) view.findViewById(R.id.bb);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_predlojenie);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager =  new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new AdapterRvPredlojenie();
        mRecyclerView.setAdapter(adapter);
    }
}