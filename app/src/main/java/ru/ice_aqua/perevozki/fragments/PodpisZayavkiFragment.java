package ru.ice_aqua.perevozki.fragments;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;

public class PodpisZayavkiFragment extends BaseFragment {

    View view;
    TextView podpisTime, podpis;


    public PodpisZayavkiFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.fragment_podpis_zayavki, container, false);

        initView("ПОДПИСЬ ЗАЯВКИ");
        init();
        time();

        return view;
    }

    public void init(){
        podpisTime = (TextView) view.findViewById(R.id.tv_time_podpis);
        podpis = (TextView) view.findViewById(R.id.tv_podpis);

        podpis.setText(R.string.article);

    }

    public void time(){
        new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long l) {
                podpisTime.setText("00:" + l / 1000);
                if(l / 1000 < 10){
                    podpisTime.setText("00:0" + l / 1000);
                }
            }

            @Override
            public void onFinish() {
                start();
                Toast.makeText(getContext(), "Пароль отправлен повторно", Toast.LENGTH_SHORT).show();
            }
        }.start();
    }

}
