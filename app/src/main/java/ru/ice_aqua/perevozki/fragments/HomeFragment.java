package ru.ice_aqua.perevozki.fragments;



import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;


import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.adapters.AdapterRvHome;

public class HomeFragment extends BaseFragment {

    View view;

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initView("ГЛАВНАЯ");
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();


        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_home);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        AdapterRvHome adapter = new AdapterRvHome(fragmentTransaction);
        mRecyclerView.setAdapter(adapter);
        LayoutInflater inflator = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_card_view, null);

        TextView textView = (TextView) v.findViewById(R.id.tv_redactor);

        Button button = (Button) view.findViewById(R.id.create_filter_login);

          button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, new CreateFilterFragment());
                fragmentTransaction.commit();
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, new RedactirovanieFiltraFragment());
                fragmentTransaction.commit();
                Toast.makeText(getContext(), "dfgdfgfd", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
