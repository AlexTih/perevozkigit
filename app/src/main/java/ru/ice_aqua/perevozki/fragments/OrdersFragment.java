package ru.ice_aqua.perevozki.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.adapters.AdapterRvVRabote;

public class OrdersFragment extends BaseFragment {


    View view;

    public OrdersFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_orders, container, false);

        initView("МОИ ЗАКАЗЫ");

        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_v_rabote);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager =  new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        AdapterRvVRabote adapter = new AdapterRvVRabote();
        mRecyclerView.setAdapter(adapter);

        return view;
    }
}
