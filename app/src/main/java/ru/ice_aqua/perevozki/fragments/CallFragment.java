package ru.ice_aqua.perevozki.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.ice_aqua.perevozki.BaseFragment;
import ru.ice_aqua.perevozki.R;

public class CallFragment extends BaseFragment {


    public CallFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initView("ЗВОНОК");
        return inflater.inflate(R.layout.fragment_call, container, false);
    }

}
