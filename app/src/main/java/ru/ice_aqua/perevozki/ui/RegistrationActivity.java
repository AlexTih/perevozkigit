package ru.ice_aqua.perevozki.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.ice_aqua.perevozki.BaseActivity;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.config.ConfigIntent;

public class RegistrationActivity extends BaseActivity {

    Button button;
    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startIntent();
            }
        });
    }

    private void init() {
        initView();
        textView.setText("РЕГИСТРАЦИЯ");
        button = (Button) findViewById(R.id.button_registration);
        editText = (EditText) findViewById(R.id.et_phone_registration);
    }

    public void startIntent(){
        Intent intent = new Intent(RegistrationActivity.this, RegistrationConfirmationActivity.class);
        intent.putExtra(ConfigIntent.PHONE_INTENT, editText.getText().toString());
        startActivity(intent);
        overridePendingTransition(R.anim.right_in,R.anim.left_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
