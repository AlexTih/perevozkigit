package ru.ice_aqua.perevozki.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import ru.ice_aqua.perevozki.BaseActivity;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.config.ConfigIntent;

public class RegistrationConfirmationActivity extends BaseActivity {

    TextView textView2, textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_confirmation);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
    }

    public void init(){
        initView();
        textView.setText("ПОДТВЕРЖДЕНИЕ РЕГИСТРАЦИИ");
        textView2 = (TextView) findViewById(R.id.tv_confirmation);
        textView3 = (TextView) findViewById(R.id.tv_time);

        Intent intent = getIntent();
        String s = intent.getStringExtra(ConfigIntent.PHONE_INTENT);

        textView2.setText("На номер " + s + " был выслан код подтверждения. Для завершения регистраиции введите присланный код");

        time(textView3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
