package ru.ice_aqua.perevozki.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import ru.ice_aqua.perevozki.BaseActivity;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.config.ConfigIntent;

public class PasswordRecoverySecondActivity extends BaseActivity {

    TextView textView2, textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery_second);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
    }

    private void init() {
        initView();
        Intent intent = getIntent();
        String s = intent.getStringExtra(ConfigIntent.PHONE_INTENT_SECOND);

        textView.setText("ВОССТАНОВЛЕНИЕ ПАРОЛЯ");
        textView2 = (TextView) findViewById(R.id.tv_password_recovery_second);
        textView3 = (TextView) findViewById(R.id.tv_time_password_recovery_second);

        textView2.setText("На номер " + s + " был выслан СМС-код подтверждения восстановления пароля");

        time(textView3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
