package ru.ice_aqua.perevozki.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import ru.ice_aqua.perevozki.BaseActivity;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.fragments.CallFragment;
import ru.ice_aqua.perevozki.fragments.ChatFragment;
import ru.ice_aqua.perevozki.fragments.DocumentiFragment;
import ru.ice_aqua.perevozki.fragments.HomeFragment;
import ru.ice_aqua.perevozki.fragments.OrdersFragment;
import ru.ice_aqua.perevozki.fragments.PredlojenieORaboteActivity;
import ru.ice_aqua.perevozki.fragments.ProfileFragment;

public class MyFiltersActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , View.OnClickListener{

    ImageView imageView, imageView2, imageView3, imageView4, imageView5;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_filters);

        init();

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_camera) {

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {
            fragmentTransaction.replace(R.id.frame, new ProfileFragment());
        } else if (id == R.id.nav_manage) {
            fragmentTransaction.replace(R.id.frame, new DocumentiFragment());

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            finish();
        }
        fragmentTransaction.commit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        switch (view.getId()){
//            case R.id.one:
//
//            case R.id.two:
//                fragmentTransaction.replace(R.id.frame, new BlankFragment());
//                break;
//            case R.id.three:
//                fragmentTransaction.replace(R.id.frame, new ThreeFragment());
//                break;
//            case R.id.four:
//                fragmentTransaction.replace(R.id.frame, new FourFragment());
//                break;
//            case R.id.five:
//                fragmentTransaction.replace(R.id.frame, new FiveFragment());
//                break;
//        }
        if(view == imageView){
            fragmentTransaction.replace(R.id.frame, new HomeFragment());
            imageView.setImageResource(R.drawable.ic_menu_gray);
            drawer.openDrawer(GravityCompat.START);
       //     setActionBarTitle("МОИ ФИЛЬТРЫ");
        }
        else {
            imageView.setImageResource(R.drawable.ic_menu_white);
        }
        if(view == imageView2){
           fragmentTransaction.replace(R.id.frame, new HomeFragment());
            imageView2.setImageResource(R.drawable.ic_home_gray);
        }
        else {
            imageView2.setImageResource(R.drawable.ic_home_white);
        }
        if(view == imageView3){
            fragmentTransaction.replace(R.id.frame, new OrdersFragment());
            imageView3.setImageResource(R.drawable.ic_v_rabote_gray);
       //     setActionBarTitle("МОИ ЗАКАЗЫ");
        }
        else {
            imageView3.setImageResource(R.drawable.ic_v_rabote_white);
        }
        if(view == imageView4){
            fragmentTransaction.replace(R.id.frame, new PredlojenieORaboteActivity());
            imageView4.setImageResource(R.drawable.ic_chat_gray);
        //    setActionBarTitle("ПРЕДЛОЖЕНИЕ О РАБОТЕ");
        }
        else {
            imageView4.setImageResource(R.drawable.ic_chat_white);
        }
        if(view == imageView5){
            imageView5.setImageResource(R.drawable.ic_call_gray);
            fragmentTransaction.replace(R.id.frame, new CallFragment());
       //     setActionBarTitle("ЗВОНОК");
        }
        else {
            imageView5.setImageResource(R.drawable.ic_call_white);
        }
        fragmentTransaction.commit();
    }

    public void init(){
//        initView();
//        setActionBarTitle("МОИ ФИЛЬТРЫ");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView t = (TextView) toolbar.findViewById(R.id.title_tool_bar);
        t.setText("МОИ ФИЛЬТРЫ");


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        imageView = (ImageView) findViewById(R.id.one);
        imageView2 = (ImageView) findViewById(R.id.two);
        imageView3 = (ImageView) findViewById(R.id.three);
        imageView4 = (ImageView) findViewById(R.id.four);
        imageView5 = (ImageView) findViewById(R.id.five);

        imageView.setOnClickListener(this);
        imageView2.setOnClickListener(this);
        imageView3.setOnClickListener(this);
        imageView4.setOnClickListener(this);
        imageView5.setOnClickListener(this);
    }

//    public void setActionBarTitle(String title){
//        textView.setText(title);
//    }

    }
