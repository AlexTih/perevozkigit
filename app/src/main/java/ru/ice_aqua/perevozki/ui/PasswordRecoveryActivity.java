package ru.ice_aqua.perevozki.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ru.ice_aqua.perevozki.BaseActivity;
import ru.ice_aqua.perevozki.R;
import ru.ice_aqua.perevozki.config.ConfigIntent;

public class PasswordRecoveryActivity extends BaseActivity {

    TextView textView2;
    EditText editText;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startIntent();
            }
        });

    }

    private void startIntent() {
        Intent intent = new Intent(PasswordRecoveryActivity.this, PasswordRecoverySecondActivity.class);
        intent.putExtra(ConfigIntent.PHONE_INTENT_SECOND, editText.getText().toString());
        startActivity(intent);
        overridePendingTransition(R.anim.right_in,R.anim.left_out);
    }

    private void init() {
        initView();
        textView.setText("ВОССТАНОВЛЕНИЕ ПАРОЛЯ");
        textView2 = (TextView) findViewById(R.id.tv_password_recovery);
        editText = (EditText) findViewById(R.id.et_password_recovery);
        button = (Button) findViewById(R.id.button_recovery);

        textView2.setText("Для восстановления пароля введите свой номер телефона");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
