package ru.ice_aqua.perevozki.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import ru.ice_aqua.perevozki.viewHolders.ViewHolderVRabote;

/**
 * Created by александр on 09.11.2017.
 */

public class AdapterRvVRabote extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderVRabote(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
