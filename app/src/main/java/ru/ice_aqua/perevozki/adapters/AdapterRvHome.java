package ru.ice_aqua.perevozki.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import ru.ice_aqua.perevozki.viewHolders.ViewHoler;

/**
 * Created by александр on 09.11.2017.
 */

public class AdapterRvHome extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    FragmentTransaction fragmentTransaction;

    public AdapterRvHome(FragmentTransaction fragmentTransaction){
        this.fragmentTransaction = fragmentTransaction;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHoler(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHoler) holder).bind(fragmentTransaction);

    }

    @Override
    public int getItemCount() {
        return 1;
    }
}