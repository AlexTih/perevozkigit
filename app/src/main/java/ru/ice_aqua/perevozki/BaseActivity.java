package ru.ice_aqua.perevozki;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;



public class BaseActivity extends AppCompatActivity {
    public TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.title_action_bar);


    }

    public void initView(){
        textView = (TextView) findViewById(R.id.mytext);

    }

    public void time(final TextView textView){
        new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long l) {
                textView.setText("00:" + l / 1000);
                if(l / 1000 < 10){
                    textView.setText("00:0" + l / 1000);
                }
            }

            @Override
            public void onFinish() {
                start();
                Toast.makeText(getApplicationContext(), "Пароль отправлен повторно", Toast.LENGTH_SHORT).show();
            }
        }.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }
}

