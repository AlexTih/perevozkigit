package ru.ice_aqua.perevozki.config;



public class ConfigIntent {
    public static final String PHONE_INTENT = "phone";
    public static final String PHONE_INTENT_SECOND = "second";
    public static final String FROM = "from";
    public static final String RADIUS = "radius";
    public static final String TO = "to";
    public static final String CURRENT_PRICE = "current";
}
