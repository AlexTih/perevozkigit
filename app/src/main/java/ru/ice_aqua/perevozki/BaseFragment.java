package ru.ice_aqua.perevozki;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.ice_aqua.perevozki.ui.MyFiltersActivity;

import static android.view.Gravity.CENTER;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by александр on 09.11.2017.
 */

public class BaseFragment extends Fragment {

    public void initView(String title) {
//        LayoutInflater inflator = (LayoutInflater) getContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View v = inflator.inflate(R.layout.title_action_bar, null);
//
//        TextView textView = (TextView) v.findViewById(R.id.mytext);
//        textView.setText(title);
////        textView.setGravity(Gravity.CENTER);
//
//
//
//        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        actionBar.setCustomView(v);


//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);



        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        TextView t = (TextView) toolbar.findViewById(R.id.title_tool_bar);
        t.setText(title);
    }
}

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        getActivity().getFragmentManager().popBackStack();
//        return true;
//    }

